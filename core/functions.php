<?php
include "protected/config.php";
include "model/film.class.php";
function get_all_films() {	

	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if(mysqli_connect_error()) {
		echo "Error connecting to database: " . mysqli_connect_error();
		exit();
	}
	
	$sql = "SELECT * FROM films";
	$films = [];
	$result = mysqli_query($con, $sql);
	
	while($row = $result->fetch_assoc()) {
		$row["ticket_status"] = get_ticket_status($row["id"], $_SESSION["id"]);
		$films[] = new Film($row);
	}
	return $films;
}

function get_film_by_id($id) {
	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if(mysqli_connect_error()) {
		echo "Error connecting to database: " . mysqli_connect_error();
		exit();
	}
	
	$sql = "SELECT * FROM films WHERE id = $id";
	$result = mysqli_query($con, $sql);	
	$row = $result->fetch_assoc();
	$row["ticket_status"] = get_ticket_status($row["id"], $_SESSION["id"]);
	$film = new Film($row);
	
	return $film;	
}

function update_film($film) {
	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (mysqli_connect_errno()) 
	{
		echo "Error connecting to database: " . mysqli_connect_error();
		exit();
	}

	$id = $film->getId();
	$name = $film->getName();
	$description = $film->getDescription();
	$rating = $film->getRating();
	$year = $film->getYear();
	$sql = "UPDATE films SET name='$name', description='$description', rating=$rating,year=$year WHERE id=$id";

	$ok = false;
	if ($con->query($sql) === TRUE) {
	    $ok = true;
	} else {
		printf("Could not execute query!");
		die();
	    $ok = false;
	}
	mysqli_close($con);
	return $ok;
}

function insert_film($film) {
	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (mysqli_connect_errno()) 
	{
		printf("Connection failed: %s\n", mysqli_connect_error());
		exit();
	}

	$id = $film->getId();
	$name = $film->getName();
	$description = $film->getDescription();
	$rating = $film->getRating();
	$year = $film->getYear();
	$sql = "INSERT INTO films (name, description, rating, year) VALUES ('$name', '$description', $rating,$year)";

	$ok = false;
	if ($con->query($sql) === TRUE) {
	    $ok = true;
	} else {
		printf("Could not execute query!");
		die();
	    $ok = false;
	}
	mysqli_close($con);
	return $ok;
}

function get_ticket_status($film_id, $user_id){
	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (mysqli_connect_errno()) 
	{
		printf("Connection failed: %s\n", mysqli_connect_error());
		exit();
	}
	$sql = "SELECT * FROM ticket_user WHERE film_id = $film_id AND user_id = $user_id";
	
	$ok = false;
	$result = $con->query($sql);
	$row = $result->fetch_assoc();

	if( $row != null) {
		$ok = true;
	}

	mysqli_close($con);
	return $ok;
}

function buy_ticket($film_id,$user_id){
	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (mysqli_connect_errno()) 
	{
		printf("Connection failed: %s\n", mysqli_connect_error());
		exit();
	}
	$sql = "SELECT * FROM tiketa.films WHERE id=$film_id";
	$result = $con->query($sql);
	$row = $result->fetch_assoc();

	//Check difference between tickets capacity and tickets bought
	$tickets_capacity = (int)$row["tickets_capacity"];
	$tickets_bought = (int)$row["tickets_bought"];
	$price_per_ticket = (int)$row["price_per_ticket"];

	if ($tickets_bought < $tickets_capacity){

		$sql = "SELECT credits FROM users WHERE id=$user_id";
		$result = $con->query($sql);
		$row = $result->fetch_assoc();
		$user_credits = (int)$row["credits"];

		if ($user_credits >= $price_per_ticket){
			$tickets_bought += 1;
			$user_credits -= $price_per_ticket;
			$_SESSION["credits"] = $user_credits;
			$sql = "UPDATE films SET tickets_bought = $tickets_bought WHERE id=$film_id";
			$result = $con->query($sql);
			if ($result) {
				$sql = "UPDATE users SET credits=$user_credits WHERE id=$user_id";
				if ($result = $con->query($sql)) {
					$sql = "INSERT INTO ticket_user (film_id, user_id) VALUES ($film_id, $user_id)";
					$result = $con->query($sql);
					if($result){
						return true;
					} else {
						printf("Couldn't update tickets_user");
					}
				} else {
					printf("Couldn't update user");
					exit();
				}
			} else {
				printf("Couldn't update tickets");
				exit();
			}
		}
	}
}

function get_reviews($id){
	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (mysqli_connect_errno()) 
	{
		printf("Connection failed: %s\n", mysqli_connect_error());
		exit();
	}
	$sql = "SELECT * FROM reviews WHERE film_id=$id";
	$result = $con->query($sql);
	return $result;
}

function get_user_review($id, $user_id){
	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (mysqli_connect_errno()) 
	{
		printf("Connection failed: %s\n", mysqli_connect_error());
		exit();
	}

	$sql = "SELECT username FROM users WHERE id=$user_id";
	$result = $con->query($sql);
	$row = $result->fetch_assoc();
	$username = $row["username"];

	$sql = "SELECT * FROM reviews WHERE film_id=$id AND user='$username'";
	$result = $con->query($sql);
	$row = $result->fetch_assoc();

	if($row){
		return true;
	}	

	return false;
}

function post_review($review_text, $user_id, $film_id, $film_rating) {
	$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	if (mysqli_connect_errno()) 
	{
		printf("Connection failed: %s\n", mysqli_connect_error());
		exit();
	}

	$sql = "SELECT rating FROM films WHERE id = $film_id";
	$result = $con->query($sql);
	$row = $result->fetch_assoc();

	$rating = (double)((int)$row["rating"] + (int)$film_rating)/2;

	$sql = "SELECT username FROM users WHERE id=$user_id";
	$result = $con->query($sql);
	$row = $result->fetch_assoc();
	$username = $row["username"];
	$date = date("Y-m-d H:i:s");
	$sql = "INSERT INTO reviews (text, user, date, film_id) VALUES ('$review_text', '$username', '$date', $film_id )";
	var_dump($sql);
	$result = $con->query($sql);
	
	if($result){
		$sql = "UPDATE films SET rating = $rating WHERE id = $film_id";
		$result = $con->query($sql);
		if ($result){
			return true;
		} 
		return false;
	}
	return false;
	


}

?>