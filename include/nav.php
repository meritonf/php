<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="/">Tiketa</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/">Ballina
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="films.php">Filmat</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="">Shfaqje</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="">Koncerte</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="">Ngjarjet</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="">Info</a>
        </li>
        <li class="nav-item">
          <li class="nav-item dropdown">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-user"></i>
              <?php
                if(isset($_SESSION["login"]) && $_SESSION["login"] === true) {
                  echo $_SESSION["username"];
                }

              ?>
            <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li style="text-align:center;"><i class="fa fa-cog fa-fw"></i>
                  <?php
                    if(isset($_SESSION["login"]) && $_SESSION["login"] === true) {
                      echo $_SESSION["credits"]." <small>Credits</small>";
                    }
                  ?>
                </li>
                <hr>
                <li style="text-align:center;"><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> <?php echo "Logout"; ?></a></li>
            </ul>
          </li>
        </li>
      </ul>
    </div>
  </div>
</nav>
