<?php

class ReviewDrawer {

	private $element;

	public function __construct(Review $element) {
		$this->element = $element;
	}

	public function draw() {
		$html = "";

		$html .= "<table class='table'>";

		$html .= "<tbody>";
		$html .= $this->element->generateSingleView();
		$html .= "</tbody>";

		$html .= "</table>";
		echo $html;
	}
}


