<?php
session_start();
if(!isset($_SESSION["login"]) || $_SESSION["login"] !== true) {
	header("Location: login.php");
	die();
}
?>
<!DOCTYPE html>
<html lang="en">
  <?php include "include/header.php"; ?>
  <?php include "core/functions.php"; ?>

  <body>
    <?php include "include/nav.php"; ?>
    <?php include "utils/form_drawer.class.php"; ?>
    <?php include "utils/reviews_drawer.class.php"; ?>
    <?php include "model/review.class.php"; ?>
    <?php
	
	$selected_film = [];
	if(isset($_GET["id"])) {
		$id = $_GET["id"];
    $selected_film = get_film_by_id($id);
    $reviews = get_reviews($id);
    $user_id = $_SESSION["id"];
    $has_user_reviewed = get_user_review($id, $user_id);
  }

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $user_id = $_SESSION["id"];

    if(isset($_POST["review_text"])) {
      $film_id = $_POST["film_id"];
      $selected_film = get_film_by_id($film_id);
      $reviews = get_reviews($film_id);
      $user_id = $_SESSION["id"];
      $has_user_reviewed = get_user_review($film_id, $user_id);
    }

    if(isset($_POST["review_text"])) {
      $review_text = $_POST["review_text"];
    }
    if(isset($_POST["rating"])) {
      $rating = $_POST["rating"];
    }

    if (!empty($review_text) && !empty($rating) && !empty($film_id) && !empty($rating)){
      $str = "Location: film_details.php?id=".$film_id."";
      $result = post_review($review_text, $user_id, $film_id, $rating);
      if ($result) {
        header($str);
      }
    }

  }



    ?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <br/>
          <h2><?php echo $selected_film->getName(); ?></h2>
          
          <?php 
            $signleViewDrawer = new FormDrawer($selected_film);
            $signleViewDrawer->draw();
          ?>

          <a href='film_edit.php?id=<?php echo $selected_film->getId(); ?>'><button class="btn btn-primary btn-sm">Edit</button></a>
        </div>
        <div class="col-lg-4">
        <br/>
          <h2>Reviews</h4>

          <?php 
            while($row=$reviews->fetch_assoc()){
              $review = new Review($row);
              $reviewDrawer = new ReviewDrawer($review);
              $reviewDrawer->draw();
            }
          ?>
          <?php
            if (!$has_user_reviewed) {
              $film_id = $selected_film->getId();
              echo "<form method=\"post\" action=\"film_details.php\">";
              echo "<input type=\"text\" name=\"film_id\" value=".$film_id." hidden>";
              echo "<label>Comment</label><input type=\"text\" class=\"form-control\" name=\"review_text\" placeholder=\"Add comment\" style=\"width:100%\"><br>";
              echo "<label>Rating</label><select class=\"form-control\" name=\"rating\" id=\"sel2\"><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option></select><br>";
              echo "<button action=\"submit\" class=\"btn btn-primary btn-sm\">Submit</button>";
              echo "</form>";
            }
          ?>
        </div>
      </div>
    </div>

    <?php include "include/scripts.php"; ?>
  </body>

</html>
