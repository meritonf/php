<?php
session_start();
if(!isset($_SESSION["login"])|| $_SESSION["login"] !== true) {
	header("Location: login.php");
	die();
}
?>
<!DOCTYPE html>
<html lang="en">
  <?php include "include/header.php"; ?>
  <?php include "core/functions.php"; ?>

  <body>
    <?php include "include/nav.php"; ?>
    <?php include "utils/table_drawer.class.php"; ?>
    <?php

      $films = get_all_films();
    ?>
    <?php
      if(isset($_GET["id"])) {
        $film_id = $_GET["id"];
        $user_id = $_SESSION["id"];
        if (buy_ticket($film_id,$user_id)){
          header("Location:films.php");
        }
      }
    ?>
		<?php
			if (!isset($_SESSION['username'])){
				header("Location:films.php");
			}

			$logged_user_name = $_SESSION['username'];
		?>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
              <?php
                $drawer = new TableDrawer(Film::getTableHeader(), $films);
                echo $drawer->draw();
              ?>
          <a href="film_edit.php">Add new</a>
        </div>
      </div>
    </div>

    <?php include "include/scripts.php"; ?>
  </body>

</html>
