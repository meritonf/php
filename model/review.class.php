<?php
include_once "model/view.interface.php";

class Review implements View {
    private $review_id;
    private $film_id;
    private $comment;
    private $user;
    private $date;


    function __construct($row = []) {
		if (!empty($row)){
            $this->review_id = $row["id"];
			$this->film_id = $row["film_id"];
			$this->comment = $row["text"];
			$this->user = $row["user"];
			$this->date = $row["date"];
		}
    }
    
    function generateSingleView(){
        $html = "";

		$html .= "<table class=\"table\">";

		$html .= "<tr>";
        $html .= "<td>";
        $html .= $this->user;
        $html .= "</td>";
		$html .= "<td>";
		$html .= $this->comment;
		$html .= "</td>";
		$html .= "</tr>";


		$html .= "</table>";

		return $html;
    }

    function generateTableRow(){}
}